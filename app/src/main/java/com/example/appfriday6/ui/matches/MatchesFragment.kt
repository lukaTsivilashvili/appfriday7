package com.example.appfriday6.ui.matches


import android.annotation.SuppressLint
import android.content.res.ColorStateList
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.appfriday6.BaseFragment
import com.example.appfriday6.adapters.ParentAdapter
import com.example.appfriday6.databinding.FragmentMatchesBinding
import com.example.appfriday6.extensions.loadImage
import com.example.appfriday6.models.MainMatch

class MatchesFragment : BaseFragment<FragmentMatchesBinding, MatchesViewModel>(
    FragmentMatchesBinding::inflate,
    MatchesViewModel::class.java
) {
    private lateinit var adapter: ParentAdapter
    override fun start(inflater: LayoutInflater, container: ViewGroup?) {
        observer()
        viewModel.getGame()
    }

    private fun observer() {
        viewModel._infoMatch.observe(viewLifecycleOwner, {
            setUpFragment(it)
            initRecycler(it)
        })
    }

    private fun initRecycler(it: MainMatch) {
        adapter = ParentAdapter(it)
        binding.recyclerParent.layoutManager =
            LinearLayoutManager(context)
        binding.recyclerParent.adapter = adapter
    }

    @SuppressLint("SetTextI18n")
    private fun setUpFragment(it: MainMatch) {
        binding.nameTeam1.text = it.match.team1.teamName
        binding.nameTeam2.text = it.match.team2.teamName
        binding.matchTime.text = it.match.matchTime.toString()
        binding.team1Posession.text = it.match.team1.ballPosition.toString() + "%"
        binding.team2Posession.text = it.match.team2.ballPosition.toString() + "%"
        binding.ballPositionProgress.progress = it.match.team1.ballPosition
        binding.ballPositionProgress.progressTintList = ColorStateList.valueOf(Color.BLUE);
        binding.ballPositionProgress.progressBackgroundTintList =
            ColorStateList.valueOf(Color.BLUE);
        binding.imageTeam1.loadImage(it.match.team1.teamImage)
        binding.imageTeam2.loadImage(it.match.team2.teamImage)
        binding.scoreTV.text =
            it.match.team1.score.toString() + " : " + it.match.team2.score.toString()
    }
}