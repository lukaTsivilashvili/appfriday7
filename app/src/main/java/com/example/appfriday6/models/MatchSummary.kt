package com.example.appfriday6.models

data class MatchSummary(
    val summaries: List<Summary>
)