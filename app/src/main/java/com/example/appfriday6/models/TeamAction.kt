package com.example.appfriday6.models

data class TeamAction(
    val action: Action,
    val actionType: Int,
    val teamType: Int?
)