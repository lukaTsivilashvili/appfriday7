package com.example.appfriday6

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding


typealias Inflate<T> = (LayoutInflater, ViewGroup?, Boolean) -> T

abstract class BaseFragment<BIN : ViewBinding, VIM : ViewModel>(
    private val inflate: Inflate<BIN>,
    private val viewModelClass: Class<VIM>
) : Fragment() {

    private var _binding: BIN? = null
    val binding get() = _binding!!

    protected open val viewModel: VIM by lazy {
        ViewModelProvider(requireActivity()).get(
            viewModelClass
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (_binding == null) {
            _binding = inflate.invoke(inflater, container, false)
        }
        start(inflater, container)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    abstract fun start(
        inflater: LayoutInflater,
        container: ViewGroup?
    )
}