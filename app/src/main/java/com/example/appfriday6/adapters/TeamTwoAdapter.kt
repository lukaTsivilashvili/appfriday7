package com.example.appfriday6.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.appfriday6.R
import com.example.appfriday6.databinding.Team2LayoutBinding
import com.example.appfriday6.extensions.loadImage
import com.example.appfriday6.matchEnums.ActionEnum
import com.example.appfriday6.matchEnums.GoalsEnum
import com.example.appfriday6.models.TeamAction

class TeamTwoAdapter(
    private val time: String,
    private val team2Action: MutableList<TeamAction?>,
    private val actionSurplus: Int = 0,
) : RecyclerView.Adapter<TeamTwoAdapter.ChildRecyclerViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChildRecyclerViewHolder {

        val itemView =
            Team2LayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        val holder = ChildRecyclerViewHolder(itemView)
        return holder
    }

    override fun onBindViewHolder(holder: ChildRecyclerViewHolder, position: Int) {
        holder.onBind()
    }

    override fun getItemCount(): Int {
        return team2Action.size + actionSurplus
    }

    inner class ChildRecyclerViewHolder(private val binding: Team2LayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun onBind() {
            if (adapterPosition >= team2Action.size) return

            val action = team2Action[adapterPosition]!!.action
            when (team2Action[adapterPosition]!!.actionType) {
                ActionEnum.GOAL.num -> {
                    if (action.goalType == GoalsEnum.OWN_GOAL.num) {
                        binding.actionImageView.setImageResource(R.drawable.path_46)
                        binding.text.text = "$time' Own Goal by"
                        binding.text.setTextColor(Color.RED);

                    } else {
                        binding.text.text = "$time' Goal by"
                        binding.actionImageView.setImageResource(R.drawable.path_44)
                    }
                    binding.playerImageView.loadImage(action.player!!.playerImage)
                    binding.team1Action.text =
                        action.player.playerName
                }
                ActionEnum.YELLOW_CARD.num -> {
                    binding.playerImageView.loadImage(action.player!!.playerImage)

                    binding.actionImageView.setImageResource(R.drawable.path_81)
                    binding.text.text = "$time' Tripping"
                    binding.team1Action.text =
                        action.player.playerName
                }
                ActionEnum.RED_CARD.num -> {
                    binding.playerImageView.loadImage(action.player!!.playerImage)
                    binding.actionImageView.setImageResource(R.mipmap.ic_launcher_round)

                    binding.text.text = "$time' Tripping"
                    binding.team1Action.text =
                        action.player.playerName
                }
            }
        }
    }
}


