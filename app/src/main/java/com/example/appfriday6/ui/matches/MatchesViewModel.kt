package com.example.appfriday6.ui.matches

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.appfriday6.models.MainMatch
import com.example.appfriday6.retrofitStuff.RetrofitService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MatchesViewModel : ViewModel() {

    private var infoMatch = MutableLiveData<MainMatch>().apply {
    }

    val _infoMatch get() = infoMatch

    fun getGame() {
        viewModelScope.launch {
            withContext(Dispatchers.Default) {
                val response = RetrofitService.service().getMatchInfo()

                if (response.isSuccessful) {
                    val body = response.body()
                    infoMatch.postValue(body)
                }
            }

        }
    }
}