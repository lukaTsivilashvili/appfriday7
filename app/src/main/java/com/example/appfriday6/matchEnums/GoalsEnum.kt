package com.example.appfriday6.matchEnums

enum class GoalsEnum(val num:Int) {

    GOAL(1),
    OWN_GOAL(2)

}