package com.example.appfriday6.retrofitStuff

import com.example.appfriday6.models.MainMatch
import retrofit2.Response
import retrofit2.http.GET

interface RetrofitRepo {
    @GET("48bb936e-261a-4471-a362-3bbb3b9a2a58/")
    suspend fun getMatchInfo(): Response<MainMatch>
}