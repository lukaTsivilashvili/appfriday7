package com.example.appfriday6.models

data class MainMatch(
    val resultCode:Int,
    val match: Match
)
