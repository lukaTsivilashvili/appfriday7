package com.example.appfriday6.models

data class Player(
    val playerImage: String? = null,
    val playerName: String
)