package com.example.appfriday6.matchEnums

enum class TeamsEnum(val num:Int) {

    TEAM1(1),
    TEAM2(2)

}