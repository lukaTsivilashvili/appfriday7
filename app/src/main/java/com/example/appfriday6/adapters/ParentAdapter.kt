package com.example.appfriday6.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appfriday6.databinding.RecyclerItemLayoutBinding
import com.example.appfriday6.models.MainMatch
import com.example.appfriday6.models.Summary
import com.example.appfriday6.models.TeamAction

class ParentAdapter(
    private var game: MainMatch,
) :
    RecyclerView.Adapter<ParentAdapter.ChildRecyclerViewHolder>() {
    private lateinit var binding: RecyclerItemLayoutBinding
    private val viewPool = RecyclerView.RecycledViewPool()

    inner class ChildRecyclerViewHolder(binding: RecyclerItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private val team1Recycler: RecyclerView = binding.team1Recycler
        private val team2Recycler: RecyclerView = binding.team2Recycler
        private lateinit var summary: Summary

        private lateinit var team1Action: MutableList<TeamAction?>
        private lateinit var team2Action: MutableList<TeamAction?>
        fun onBind() {
            team1Action = mutableListOf()
            team2Action = mutableListOf()

            summary = game.match.matchSummary.summaries[adapterPosition]
            populateTeamActions()
            val team1recyclerLayoutManager = LinearLayoutManager(
                team1Recycler.context, LinearLayoutManager.VERTICAL, false
            )
            team1recyclerLayoutManager.initialPrefetchItemCount = 4
            team1Recycler.apply {
                layoutManager = team1recyclerLayoutManager
                adapter = TeamOneAdapter(
                    summary.actionTime, team1Action,
                    if (team1Action.size > team2Action.size) 0 else team2Action.size - team1Action.size
                )
                setRecycledViewPool(viewPool)
                isNestedScrollingEnabled = false
            }

            val team2recyclerLayoutManager = LinearLayoutManager(
                team2Recycler.context, LinearLayoutManager.VERTICAL, false
            )
            team2recyclerLayoutManager.initialPrefetchItemCount = 4
            team2Recycler.apply {
                layoutManager = team2recyclerLayoutManager
                adapter = TeamTwoAdapter(
                    summary.actionTime, team2Action,
                    if (team1Action.size < team2Action.size) 0 else team1Action.size - team2Action.size
                )
                setRecycledViewPool(viewPool)
                isNestedScrollingEnabled = false

            }
        }

        private fun populateTeamActions() {
            if (summary.team1Action != null) {
                team1Action.addAll(summary.team1Action!!)
            }
            if (summary.team2Action != null) {
                team2Action.addAll(summary.team2Action!!)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChildRecyclerViewHolder {
        binding =
            RecyclerItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ChildRecyclerViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ChildRecyclerViewHolder, position: Int) {
        holder.onBind()
    }

    override fun getItemCount(): Int {
        return game.match.matchSummary.summaries.size
    }
}