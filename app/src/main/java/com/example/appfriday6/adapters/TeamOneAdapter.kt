package com.example.appfriday6.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.appfriday6.R
import com.example.appfriday6.databinding.Team1LayoutBinding
import com.example.appfriday6.extensions.loadImage
import com.example.appfriday6.matchEnums.ActionEnum
import com.example.appfriday6.matchEnums.GoalsEnum
import com.example.appfriday6.models.TeamAction

class TeamOneAdapter(
    private val time: String,
    private val team1Action: MutableList<TeamAction?>,
    private val actionSurplus: Int = 0,
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private lateinit var binding: Team1LayoutBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        binding = Team1LayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ChildRecyclerViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ChildRecyclerViewHolder).onBind()
    }

    override fun getItemCount(): Int {
        return team1Action.size + actionSurplus
    }

    inner class ChildRecyclerViewHolder(binding: Team1LayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun onBind() {
            if (adapterPosition >= team1Action.size) return
            val action = team1Action[adapterPosition]!!.action
            when (team1Action[adapterPosition]!!.actionType) {
                ActionEnum.GOAL.num -> {
                    if (action.goalType == GoalsEnum.OWN_GOAL.num) {
                        binding.actionImageView.setImageResource(R.drawable.path_46)
                        binding.text.text = "$time' Own Goal by"
                        binding.text.setTextColor(Color.RED);

                    } else {
                        binding.text.text = "$time' Goal by"
                        binding.actionImageView.setImageResource(R.drawable.path_44)
                    }
                    binding.playerImageView.loadImage(action.player!!.playerImage)
                    binding.team1Action.text =
                        action.player.playerName
                }
                ActionEnum.SUBSTITUTION.num -> {
                    binding.playerImageView.loadImage(action.player1!!.playerImage)

                    binding.actionImageView.setImageResource(R.drawable.group_56)
                    binding.text.text = "$time' Substitution"
                    binding.team1Action.text =
                        action.player1.playerName
                }
                ActionEnum.YELLOW_CARD.num -> {
                    binding.actionImageView.setImageResource(R.drawable.path_81)
                    binding.text.text = "$time' Tripping"
                    binding.playerImageView.loadImage(action.player!!.playerImage)
                    binding.team1Action.text =
                        action.player.playerName
                }
                ActionEnum.RED_CARD.num -> {
                    binding.playerImageView.loadImage(action.player!!.playerImage)
                    binding.text.text = "$time' Tripping"
                    binding.team1Action.text =
                        action.player.playerName
                }
            }
        }

    }
}
