package com.example.appfriday6.extensions

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.appfriday6.R

fun ImageView.loadImage(imageUrl: String?) {
    Glide.with(this)
        .load(imageUrl)
        .apply(RequestOptions.placeholderOf(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher))
        .centerCrop().into(this)
}