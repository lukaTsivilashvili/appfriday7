package com.example.appfriday6.models

data class Action(
    val goalType: Int? = null,
    val player: Player? = null,
    val player1: Player? = null,
    val player2: Player? = null
)