package com.example.appfriday6.models

data class Team(
    val ballPosition: Int,
    val score: Int,
    val teamImage: String,
    val teamName: String
)