package com.example.appfriday6.matchEnums

enum class ActionEnum(val num: Int) {
    GOAL(1),
    YELLOW_CARD(2),
    RED_CARD(3),
    SUBSTITUTION(4)
}