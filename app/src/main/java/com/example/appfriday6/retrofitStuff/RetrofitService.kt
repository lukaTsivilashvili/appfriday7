package com.example.appfriday6.retrofitStuff

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory

object RetrofitService {

    private const val BASE_URL = "https://run.mocky.io/v3/"

    fun service(): RetrofitRepo {
        return Retrofit.Builder().baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create()).build()
            .create(RetrofitRepo::class.java)
    }

}