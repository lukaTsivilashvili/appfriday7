package com.example.appfriday6.models

data class Summary(
    val actionTime: String,
    val team1Action: List<TeamAction?>?,
    val team2Action: List<TeamAction?>?
)